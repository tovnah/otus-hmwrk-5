package ru.otus.homework.hmwrk2;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "profile_entity")
public class Profile {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String login;
    private String email;
    private String firstName;
    private String lastName;
    @JsonIgnore
    private String password;
}
