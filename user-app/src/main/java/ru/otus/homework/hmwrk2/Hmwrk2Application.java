package ru.otus.homework.hmwrk2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hmwrk2Application {

    public static void main(String[] args) {
        SpringApplication.run(Hmwrk2Application.class, args);
    }

}
