package ru.otus.homework.hmwrk2;

import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.distribution.DistributionStatisticConfig;
import io.prometheus.client.CollectorRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

import java.time.Duration;

@Configuration
public class ConfigurationControllers {

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return RepositoryRestConfigurer.withConfig(config -> {
            config.exposeIdsFor(User.class);
        });
    }

    @Bean
    public CollectorRegistry collectorRegistry() {
        return CollectorRegistry.defaultRegistry;
    }

    @Bean
    public MeterFilter meterFilter() {
        return MeterFilter.maxExpected("http.server.requests", Duration.ofSeconds(1));
    }

    @Bean
    public MeterFilter meterFilter1() {
        return MeterFilter.minExpected("http.server.reqests", Duration.ofMillis(10));
    }

//    @Bean
//    JvmThreadMetrics threadMetrics() {
//        return new JvmThreadMetrics();
//    }
//
//    @Bean
//    MeterRegistry metricsCommonTags() {
//        final CompositeMeterRegistry registry = new CompositeMeterRegistry();
//        registry.config().meterFilter(new MeterFilter() {
//            @Override
//            public DistributionStatisticConfig configure(Meter.Id id, DistributionStatisticConfig config) {
//                if (id.getName().equals("http.request.duration")) {
//                    return DistributionStatisticConfig.builder()
//                            .sla(Duration.ofMillis(10).toNanos(),
//                                    Duration.ofMillis(25).toNanos(),
//                                    Duration.ofMillis(50).toNanos(),
//                                    Duration.ofMillis(100).toNanos(),
//                                    Duration.ofMillis(500).toNanos(),
//                                    Duration.ofMillis(1000).toNanos(),
//                                    Duration.ofMillis(5000).toNanos())
//                            .build()
//                            .merge(config);
//                }
//                return config;
//            }
//        });
//
//        return registry;
//    }
}
