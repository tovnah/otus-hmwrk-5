package ru.otus.homework.hmwrk2;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "user_entity")
public class User {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    private String name;
}
