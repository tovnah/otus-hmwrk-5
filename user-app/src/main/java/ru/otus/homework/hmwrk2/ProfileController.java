package ru.otus.homework.hmwrk2;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.util.NumberUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Slf4j
@RestController("ProfileControllerImpl")
public class ProfileController {
    @PostMapping(value = "/info")
    public Profile info(@RequestHeader Map<String, String> headers, HttpServletResponse response) {
        log.info("Headers {}", headers);
        if (Strings.isEmpty(headers.get("x-userid"))) {
            response.setStatus(401);
            return null;
        }

        Profile profile = new Profile();

        profile.setId(NumberUtils.parseNumber(headers.get("x-userid"), Long.class));
        profile.setLogin(headers.get("x-userlogin"));
        profile.setEmail(headers.get("x-useremail"));
        profile.setFirstName(headers.get("x-userfirstname"));
        profile.setLastName(headers.get("x-userlastname"));

        return profile;
    }
}
