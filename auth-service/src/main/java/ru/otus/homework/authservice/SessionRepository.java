package ru.otus.homework.authservice;

import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session, String> {
    void deleteById(String id);

}
