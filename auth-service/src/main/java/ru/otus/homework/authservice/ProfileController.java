package ru.otus.homework.authservice;

import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.NumberUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.LogRecord;

@Slf4j
@RestController("profile")
public class ProfileController {


    @PostMapping(value = "/register")
    public Profile register(@RequestBody Profile profile, HttpServletResponse response) {
        Profile savesProfile = profilesRepository.save(profile);

        return savesProfile;
    }

    @PostMapping(value = "/login")
    public LoginResponse login(@RequestBody LoginRequest request, HttpServletResponse response) {
        Profile responseProfile = profilesRepository.findByLoginAndPassword(request.getLogin(), request.getPassword());
        if (responseProfile == null) {
            response.setStatus(401);
            return null;
        }

        String sessionId = UUID.randomUUID().toString();
        sessionRepository.save(new Session(
                sessionId,
                responseProfile
        ));

        Cookie cookie = new Cookie("session_id", sessionId);
        cookie.setMaxAge(3600);
        cookie.setHttpOnly(true);
        response.addCookie(cookie);

        return new LoginResponse(sessionId);
    }

    @PostMapping(value = "/change")
    public Profile change(@RequestBody Profile profile, @RequestHeader Map<String, String> headers, HttpServletRequest httpServletRequest, HttpServletResponse response) {
        Cookie session_id = WebUtils.getCookie(httpServletRequest, "session_id");
        if (session_id == null || Strings.isEmpty(session_id.getValue())) {
            response.setStatus(401);
            return null;
        }

        log.info("Request with Cookie {}", session_id.getValue());
        log.info("Headers {}", headers);

        Optional<Session> optionalSession = sessionRepository.findById(session_id.getValue());
        if (!optionalSession.isPresent()) {
            response.setStatus(401);
            return null;
        }
        Session session = optionalSession.get();
        if (!Objects.equals(profile.getId(), session.getProfile().getId())) {
            response.setStatus(403);
            return null;
        }

        Profile savedProfile = profilesRepository.save(profile);
        return savedProfile;
    }

    @RequestMapping(value = "/auth", method = {RequestMethod.GET, RequestMethod.POST})
    public void auth(HttpServletRequest httpServletRequest, HttpServletResponse response) {
        Cookie session_id = WebUtils.getCookie(httpServletRequest, "session_id");
        if (session_id == null || Strings.isEmpty(session_id.getValue())) {
            response.setStatus(401);
            return;
        }

        log.info("Request with Cookie {}", session_id.getValue());
        Optional<Session> optionalSession = sessionRepository.findById(session_id.getValue());
        if (!optionalSession.isPresent()) {
            response.setStatus(401);
            return;
        }

        Profile profile = optionalSession.get().getProfile();
        response.addHeader("x-userid", profile.getId().toString());
        response.addHeader("x-userlogin", profile.getLogin());
        response.addHeader("x-useremail", profile.getEmail());
        response.addHeader("x-userfirstname", profile.getFirstName());
        response.addHeader("x-userlastname", profile.getLastName());
    }


    @GetMapping(value = "/signin")
    public Map<String, String> signin() {
        HashMap<String, String> response = new HashMap<>();
        response.put("message", "Please go to login and provide Login/Password");

        return response;
    }

    @GetMapping(value = "/sessions")
    public Iterable<Session> sessions() {
        return sessionRepository.findAll();
    }

    @GetMapping(value = "/profiles")
    public Iterable<Profile> profiles() {
        return profilesRepository.findAll();
    }

    @PostMapping(value = "/logout")
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse response) {
        Cookie session_id = WebUtils.getCookie(httpServletRequest, "session_id");
        if (session_id == null || Strings.isEmpty(session_id.getValue())) {
            return;
        }

        if (!sessionRepository.findById(session_id.getValue()).isPresent()) {
            return;
        }

        sessionRepository.deleteById(session_id.getValue());
        Cookie cookie = new Cookie("session_id", null); // Not necessary, but saves bandwidth.
//        cookie.setPath("/MyApplication");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0); // Don't set to -1 or it will become a session cookie!
        response.addCookie(cookie);
    }


    @Autowired
    private ProfileRepository profilesRepository;
    @Autowired
    private SessionRepository sessionRepository;

//    private static final ConcurrentHashMap<String, Profile> STRING_PROFILE_MAP = new ConcurrentHashMap<>();
}
