package ru.otus.homework.authservice;

import org.springframework.data.repository.CrudRepository;

public interface ProfileRepository extends CrudRepository<Profile, Long> {
    Profile findByLoginAndPassword(String login, String password);
}
