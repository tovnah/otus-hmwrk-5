package ru.otus.homework.authservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNullApi;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "session_entity")
public class Session {
    @Id
    private String id;
    @OneToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;
}
