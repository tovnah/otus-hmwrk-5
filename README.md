Домашнее задание `Backend for frontends. Apigateway`
- запустить user-app ```helm install otus-hmwrk-5-app ./otus-hmwrk-2-chart/```
- запустить auth-service ```helm install otus-hmwrk-5-auth-service ./auth-service-chart/```
- выполнить проверку сценария ```newman run postman/otus_homework5.postman_collection.json ```

![diagramm.png](./README.assets/diagramm.png)